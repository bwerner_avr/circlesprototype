﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetBehavior : MonoBehaviour
{
    private CircleRaycast raycastManager;
    private Image thisImg;
    public bool lit;
    public bool fading;

    // Start is called before the first frame update
    void Start()
    {
        raycastManager = FindObjectOfType<CircleRaycast>();
        thisImg = GetComponent<Image>();
        thisImg.color = raycastManager.defaultColor;
    }

    public void Behavior()
    {
        if (!lit)
        {
            lit = true;
            StartCoroutine(_FadeColor(thisImg, 1f));
        }
    }

    protected IEnumerator _FadeColor(Image img, float fadeTime)
    {
        fading = true;
        var start = Time.time;
        var elapsed = 0f;
        var normalizedTime = 0f;
        var startValue = raycastManager.defaultColor;
        var endValue = raycastManager.hitColor;
        //endValue.a = 0f;
        while (elapsed < fadeTime)
        {
            yield return null;
            elapsed = Time.time - start;
            normalizedTime = Mathf.Clamp(elapsed / fadeTime, 0f, 1f);
            img.color = Color.Lerp(startValue, endValue, normalizedTime);
        }
        yield return new WaitForSeconds(.25f);
        fading = false;
        //StartCoroutine(_FadeOutColor(thisImg, 1f));
    }

    public void FadeOut()
    {
        StartCoroutine(_FadeOutColor(thisImg, 1f));
    }
    
    public IEnumerator _FadeOutColor(Image img, float fadeTime)
    {
        fading = true;
        var start = Time.time;
        var elapsed = 0f;
        var normalizedTime = 0f;
        var startValue = raycastManager.hitColor;
        var endValue = raycastManager.defaultColor;
        while (elapsed < fadeTime)
        {
            yield return null;
            elapsed = Time.time - start;
            normalizedTime = Mathf.Clamp(elapsed / fadeTime, 0f, 1f);
            img.color = Color.Lerp(startValue, endValue, normalizedTime);
        }
        fading = false;
        lit = false;
    }
    
}
