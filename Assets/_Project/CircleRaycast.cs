﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircleRaycast : MonoBehaviour
{
    public GameObject head;
    private Transform rayOrigin;
    public Image[] targets;
    public Color hitColor;
    public Color defaultColor;
    public GameObject cursor;
    void Update()
    {
        rayOrigin = head.transform;

        targets = FindObjectsOfType<Image>();

        // Bit shift the index of the layer (8) to get a bit mask
        int layerMask = 1 << 9;

        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(rayOrigin.position, rayOrigin.transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
        {
            Debug.DrawRay(rayOrigin.position, rayOrigin.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            Debug.Log("Did Hit");
            //hit.transform.GetComponent<Image>().color = hitColor;
            cursor.SetActive(true);
            cursor.transform.position = hit.point;
            var behavior = hit.transform.GetComponent<TargetBehavior>();
            behavior.Behavior();
            behavior.lit = true;

        }
        else
        {
            Debug.DrawRay(rayOrigin.position, rayOrigin.TransformDirection(Vector3.forward) * 1000, Color.white);
            Debug.Log("Did not Hit");
            cursor.SetActive(false);
            foreach (Image img in targets)
            {
                //img.color = defaultColor;
                var bevahior = img.gameObject.GetComponent<TargetBehavior>();
                if (!bevahior.fading && bevahior.lit)
                {
                    bevahior.FadeOut();
                }

            }
        }
    }
}
